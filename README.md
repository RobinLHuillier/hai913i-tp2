# CodeProbe  
*Application d'analyse statique de code*

---

HAI913I - TP2 - L'Huillier - Urena

---
### Lancer en ligne de commande : 

Se placer dans le dossier "hai913i-tp2" pour la première partie du tp, ou dans le dossier "hai913i-tp2-spoon" pour la seconde, puis :

#### Installer : 
``mvn clean install``

#### Lancer l'interface graphique : 
``mvn exec:java

### Lancer automatiquement : 

#### Sous windows : double cliquer sur le fichier ``run-tp2-vanilla.bat`` (première partie) ou ``run-tp2-spoon.bat`` (seconde partie)

#### Sous linux : lancer ``run-tp2-vanilla.sh`` (première partie) ou ``run-tp2-spoon.sh`` (seconde partie)

---

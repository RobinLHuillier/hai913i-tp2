package exceptions;

public class ProcessorNotExecutedException extends Exception {
	public ProcessorNotExecutedException() {}
	
	public ProcessorNotExecutedException(String message) {
		super(message);
	}
}

package processors;

import java.util.HashMap;
import java.util.Map;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.filter.TypeFilter;

public class MethodProcessor extends AbstractProcessor<CtMethod<?>>{
	private Map<String, Map<String, Integer>> classRelations = new HashMap<>();
	private boolean isExecuted;
	
	@Override
	public void process(CtMethod<?> element) {
		this.isExecuted = true;
		
		//on parcourt pour chaque methode les methodes qu'elle invoque
		for(CtInvocation<?> invoc: element.getElements(new TypeFilter<>(CtInvocation.class))) {
			String callingClass = element.getParent(CtType.class).getQualifiedName();
			CtTypeReference<?> typeRef = invoc.getExecutable().getDeclaringType();
			if(typeRef != null) {
				String calledClass = invoc.getExecutable().getDeclaringType().getQualifiedName();			
				//on s'assure que chaque paire de classe à un compteur mis à jour dans classRelations
				classRelations.putIfAbsent(callingClass, new HashMap<>());
				classRelations.get(callingClass).put(calledClass, classRelations.get(callingClass).getOrDefault(calledClass, 0) + 1);
			}
		}
	}
	
	public Map<String, Map<String, Integer>> getClassRelations(){
		return this.classRelations;
	}
	
	
	public float calculCoupling(String a, String b) {
		int betweenAandB = classRelations.getOrDefault(a, new HashMap<>()).getOrDefault(b, 0);
		int betweenBandA = classRelations.getOrDefault(b, new HashMap<>()).getOrDefault(a, 0);
		int totalRelations = classRelations.values().stream().mapToInt(subMap -> 
			subMap.values().stream().mapToInt(Integer::intValue).sum()).sum();
		
		if(totalRelations > 0) {
			return ((float) (betweenAandB + betweenBandA) / totalRelations); 
		}
		
		return 0;
	}
	
	public boolean isExecuted() {
		return this.isExecuted;
	}
	
}

package app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;

import exceptions.ProcessorNotExecutedException;
import models.HierarchicalClustering;
import models.SpoonCouplageGraph;
import models.Cluster;
import models.InitialCluster;
import models.ElemCluster;
import spoon.Launcher;
import spoon.reflect.declaration.CtType;
import processors.MethodProcessor;

public class CodeProbeController implements CodeProbeI {
	private Launcher launcher;
	private String projectPath;
	private MethodProcessor methodProcessor;
	private HierarchicalClustering clustering;
	private ElemCluster dendroRoot;

	public CodeProbeController() {}

	public void setProjectPath(String path) {
		this.projectPath = path;
	}

	public void setJrePath(String path) {}

	@Override
	public void initialize() {
		launcher = new Launcher();
		
		String srcPath;
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.contains("win")) {
			srcPath = "\\src";
		} else {	
			srcPath = "/src";
		}
		
		launcher.getEnvironment().setComplianceLevel(17);
		launcher.addInputResource(projectPath + srcPath);	
		
		methodProcessor = new MethodProcessor();
		launcher.addProcessor(methodProcessor);
		launcher.run();
		
		if(!methodProcessor.isExecuted()) {
			System.out.println("Erreur: methodProcesseur n'a pas été executé");
			return;
		}
		
		this.clustering = null;
		this.dendroRoot = null;
	}

	public List<String> getAllClasses() {
		return new ArrayList<>(methodProcessor.getClassRelations().keySet());
	}

	public float getCouplage(String classA, String classB) {
		return methodProcessor.calculCoupling(classA, classB);
	}

	public mxGraph getCouplageGraph() {
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		graph.getModel().beginUpdate();
		
		Map<String, Object> vertexes = new HashMap<String, Object>();
		for (String className: this.getAllClasses()) {
			Object v = graph.insertVertex(parent, null, className, 0, 0, className.length()*7, 30);
			vertexes.put(className, v);
		}
		
		for(String a: methodProcessor.getClassRelations().keySet()) {
			Object v1 = vertexes.get(a);
			for(String b: methodProcessor.getClassRelations().get(a).keySet()) {
				Object v2 = vertexes.get(b);
				CtType<?> typeA = launcher.getFactory().Type().get(a);
				CtType<?> typeB = launcher.getFactory().Type().get(b);
				if(typeA != null && typeB != null) {
				    float couplage = methodProcessor.calculCoupling(a, b);
				    if (couplage > 0) {
						graph.insertEdge(parent, null, String.format("%.3f", couplage), v1, v2);
					}
				}
			}
		}
		
		graph.getModel().endUpdate();
		return graph;
	}

	public mxGraph getClustering() {
		checkClustering();
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		if (this.dendroRoot == null) {
			return graph;
		}
		
		graph.getModel().beginUpdate();
		
		Map<ElemCluster, Object> vertexes = new HashMap<ElemCluster, Object>();
		recursiveInsertVertex(vertexes, graph, dendroRoot, parent);
		
		recursiveInsertEdge(vertexes, graph, dendroRoot, parent);
		
		graph.getModel().endUpdate();
		return graph;
	}

	public mxGraph getModulePartition(float cp) {
		checkClustering();
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		if (this.dendroRoot == null) {
			return graph;
		}
		
		List<ElemCluster> modules = clustering.identifyModules(dendroRoot, cp);
		System.out.println(modules.size());
		
		graph.getModel().beginUpdate();
		
		Map<ElemCluster, Object> vertexes = new HashMap<ElemCluster, Object>();
		for (ElemCluster c: modules) {
			recursiveInsertVertex(vertexes, graph, c, parent);
			
			recursiveInsertEdge(vertexes, graph, c, parent);
		}
		
		graph.getModel().endUpdate();
		return graph;
	}
	
	private void recursiveInsertVertex(Map<ElemCluster, Object> vertexes, mxGraph graph, ElemCluster cluster, Object parent) {
		String name = "";
		if (cluster instanceof InitialCluster) {
			name = ((InitialCluster)cluster).getClasses().get(0).getQualifiedName();
		}
		Object v = graph.insertVertex(parent, null, name, 0, 0, name.length()*7, 30);
		vertexes.put(cluster, v);
		
		if (cluster instanceof InitialCluster) {
			return;
		}
		
		Cluster c = (Cluster)cluster;
		
		recursiveInsertVertex(vertexes, graph, c.getElemA(), parent);
		recursiveInsertVertex(vertexes, graph, c.getElemB(), parent);
	}
	
	private void recursiveInsertEdge(Map<ElemCluster, Object> vertexes, mxGraph graph, ElemCluster cluster, Object parent) {
		if (cluster instanceof InitialCluster) {
			return;
		}
		
		Cluster c = (Cluster)cluster;
		ElemCluster a = c.getElemA();
		ElemCluster b = c.getElemB();
		
		Object vc = vertexes.get(c);
		Object va = vertexes.get(a);
		Object vb = vertexes.get(b);
		
		Object e1 = graph.insertEdge(parent, null, "", vc, va);
		Object e2 = graph.insertEdge(parent, null, "", vc, vb);
		
		graph.setCellStyle("endArrow=none", new Object[]{e1});
		graph.setCellStyle("endArrow=none", new Object[]{e2});
		
		recursiveInsertEdge(vertexes, graph, a, parent);
		recursiveInsertEdge(vertexes, graph, b, parent);
	}
	
	private void checkClustering() {
		if (this.clustering == null || this.dendroRoot == null) {
			this.clustering = new HierarchicalClustering(methodProcessor);
			try {
				this.dendroRoot = this.clustering.cluster(launcher);
			} catch (ProcessorNotExecutedException e) {
				this.dendroRoot = null;
			}
		}
	}
}

package app;

import java.util.List;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import com.mxgraph.view.mxGraph;

public interface CodeProbeI {
	// setup
	public void setProjectPath(String path);
	public void setJrePath(String path);
	public void initialize();
	// Ex1Q1
	public List<String> getAllClasses();
	public float getCouplage(String classA, String classB);
	// Ex1Q2
	public mxGraph getCouplageGraph();
	// Ex2Q1
	public mxGraph getClustering();
	// Ex2Q2
	public mxGraph getModulePartition(float cp);
}

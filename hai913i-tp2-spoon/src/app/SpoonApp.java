package app;

public class SpoonApp {

	public static void main(String[] args) {
		CodeProbeI controller = new CodeProbeController();
		
		GUI gui = new GUI(controller);
		gui.start();
	}
}

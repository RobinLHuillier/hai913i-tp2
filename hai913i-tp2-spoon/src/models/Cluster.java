package models;

import java.util.List;

import spoon.reflect.declaration.CtType;

public class Cluster implements ElemCluster {
    private ElemCluster elemA;
    private ElemCluster elemB;
    private double clusterValue = 0;

    public Cluster() {}

    public Cluster(ElemCluster a, ElemCluster b, double v) {
        this.elemA = a;
        this.elemB = b;
        this.clusterValue = v;
    }

    public ElemCluster getElemA() {
        return elemA;
    }

    public void setElemA(ElemCluster elemA) {
        this.elemA = elemA;
    }

    public ElemCluster getElemB() {
        return elemB;
    }

    public void setElemB(ElemCluster elemB) {
        this.elemB = elemB;
    }   

    public double getClusterValue() {
        return this.clusterValue;
    }

    public void setClusterValue(double value) {
        this.clusterValue = value;
    }

    @Override
    public List<CtType<?>> getClasses() {
        List<CtType<?>> a = elemA.getClasses();
        a.addAll(elemB.getClasses());
        return a;
    }


    @Override
    public int countCallsToClasses(List<CtType<?>> classes) {
        return elemA.countCallsToClasses(classes) + elemB.countCallsToClasses(classes);
    }

    @Override
    public String toString() {
        return "(" + getClusterValue() + " : " + elemA.toString() + " - " + elemB.toString() + " )";
    }
}


package models;

import java.util.ArrayList;
import java.util.List;

import spoon.reflect.declaration.CtType;

public class InitialCluster implements ElemCluster {
	private CtType<?> type;

    public InitialCluster(CtType<?> type) {
        this.type = type;
    }

    @Override
    public List<CtType<?>> getClasses() {
        List<CtType<?>> classes = new ArrayList<>();
        classes.add(type);
        return classes;
    }


    @Override
    public int countCallsToClasses(List<CtType<?>> classes) {
        return 0;
    }
}

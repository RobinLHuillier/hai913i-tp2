package models;

import java.util.List;

import spoon.reflect.declaration.CtType;


public interface ElemCluster {
	public List<CtType<?>> getClasses();
	public int countCallsToClasses(List<CtType<?>> classes);
}

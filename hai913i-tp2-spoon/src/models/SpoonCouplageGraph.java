package models;

import java.util.HashMap;
import java.util.Map;

import spoon.reflect.declaration.CtType;

public class SpoonCouplageGraph {
	private Map<CtType<?>, Map<CtType<?>, Float>> graph;
	
	public SpoonCouplageGraph() {
		graph = new HashMap<>();
	}
	
	public void add(CtType<?> classA, CtType<?> classB, Float couplage) {
		graph.computeIfAbsent(classA, k -> new HashMap<>()).put(classB, couplage);
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		
		for (CtType<?> classA: graph.keySet()) {
			for (CtType<?> classB: graph.get(classA).keySet()) {
				float couplage = graph.get(classA).get(classB);
				s.append("Couplage entre " + classA.getQualifiedName() + " et " + classB.getQualifiedName() + " : " + couplage + "\n");
			}
		}
		
		return s.toString();
	}
}

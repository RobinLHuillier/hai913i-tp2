package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import exceptions.ProcessorNotExecutedException;
import processors.MethodProcessor;
import spoon.Launcher;
import spoon.reflect.declaration.CtType;

public class HierarchicalClustering {
	
    private MethodProcessor methodProcessor;

    public HierarchicalClustering(MethodProcessor methodProcessor) {
        this.methodProcessor = methodProcessor;
    }
    
	public ElemCluster cluster(Launcher launcher) throws ProcessorNotExecutedException {
        List<ElemCluster> clusters = new ArrayList<>();

        // on récupère les infos du code
        for (CtType<?> ctType : launcher.getModel().getAllTypes()) {
            clusters.add(new InitialCluster(ctType));
        }

        while (clusters.size() > 1) {
            Pair<ElemCluster, ElemCluster> closestPair = findClosestClusters(clusters);
            ElemCluster mergedCluster = new Cluster(closestPair.first, closestPair.second, computeCoupling(closestPair.first, closestPair.second));
            
            clusters.remove(closestPair.first);
            clusters.remove(closestPair.second);
            clusters.add(mergedCluster);
        }

        // 0 = la racine
        return clusters.get(0);
	}
	
	private Pair<ElemCluster, ElemCluster> findClosestClusters(List<ElemCluster> clusters) throws ProcessorNotExecutedException {
        ElemCluster closestA = null;
        ElemCluster closestB = null;
        double maxCoupling = Integer.MIN_VALUE;

        for (int i = 0; i < clusters.size(); i++) {
            for (int j = i + 1; j < clusters.size(); j++) {
                double coupling = computeCoupling(clusters.get(i), clusters.get(j));
                if (coupling > maxCoupling) {
                    maxCoupling = coupling;
                    closestA = clusters.get(i);
                    closestB = clusters.get(j);
                }
            }
        }
        
        return new Pair<>(closestA, closestB);
    }
	
	private double computeCoupling(ElemCluster a, ElemCluster b) throws ProcessorNotExecutedException {
		double totalCoupling = 0;
		
		//on s'assure que le processor a déjà été appelé pour pouvoir récupérer les couplages
		if(!methodProcessor.isExecuted()) {
			throw new ProcessorNotExecutedException("MethodProcessor n'a pas encore été appelé");
		}
		
	    for (CtType<?> classA : a.getClasses()) {
	        for (CtType<?> classB : b.getClasses()) {
	            totalCoupling += methodProcessor.calculCoupling(classA.getQualifiedName(), classB.getQualifiedName());
	        }
	    }
		
		return totalCoupling;	
	}
	
	
	public List<ElemCluster> identifyModules(ElemCluster dendroRoot, float CP){
		int M = dendroRoot.getClasses().size();
		List<ElemCluster> modules = new ArrayList<>();
		Queue<ElemCluster> queue = new LinkedList<>();
		queue.add(dendroRoot);
		
		while(!queue.isEmpty()) {
			//on récupère la tête de la queue de notre liste
			ElemCluster current = queue.poll();
			
			if(modules.size() > M/2) {
				break;
			}
			
			float average = avgCoupling(current);
			System.out.println("Moyenne du couplage pour le cluster " + current + " : " + average);

			
			//si le noeud est une feuille
			if(average > CP) {
				modules.add(current);
			}else {
				if (!children(current).isEmpty())
					queue.addAll(children(current));
			}
		}
		
		
		return modules;
	}
	
	private float avgCoupling(ElemCluster cluster) {
		List<CtType<?>> classes = cluster.getClasses();
		float total = 0;
		int cpt = 0;
		
		for(int i=0; i<classes.size(); i++) {
			for(int j=i+1; j<classes.size(); j++) {
				total += this.methodProcessor.calculCoupling(classes.get(i).getQualifiedName(), classes.get(j).getQualifiedName());
				cpt++;
			}
		}
		
		return cpt == 0 ? 0 : total / cpt;
	}
	
	
	private List<ElemCluster> children(ElemCluster cluster) {
	    if (cluster instanceof Cluster) {
	        return Arrays.asList(((Cluster) cluster).getElemA(), ((Cluster) cluster).getElemB());
	    }
	    return Collections.emptyList();
	}
	
	
	// on en aura besoin qu'ici
	private static class Pair<T, U> {
        public final T first;
        public final U second;

        public Pair(T first, U second) {
            this.first = first;
            this.second = second;
        }
    }
}

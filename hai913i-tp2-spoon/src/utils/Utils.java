package utils;

import org.eclipse.jdt.core.dom.TypeDeclaration;

public class Utils {
	public static boolean checkEqualityTypeDeclaration(TypeDeclaration classA, TypeDeclaration classB) {
		if (classA == null || classB == null) {
			return classA == classB;
		}
	    return classA.resolveBinding().equals(classB.resolveBinding());
	}
}

package app;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.DefaultListCellRenderer;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import com.mxgraph.view.mxGraph;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;

public class GUI {
	private CodeProbeI controller;
	private String projectPath;
	
	private JFrame frame;
	private JPanel page;
	
	private TypeDeclaration selectedClassA;
	private TypeDeclaration selectedClassB;
	private JLabel couplageLabel;
	
	private JTextField cpField;
	
	public GUI(CodeProbeI controller) {
		this.controller = controller;
		projectPath = new File(".").getAbsolutePath();
		this.controller.setProjectPath(projectPath);
		this.controller.initialize();
	}
	
	private void creerMenuPrincipal() {
		resetPage();
		addTitle(); 
		
        // affichage projet en cours
        JLabel lPath = new JLabel(projectPath, JLabel.LEFT);
        lPath.setBounds(200, 50, 950, 30);
        lPath.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lPath);
        
        // bouton de sélection de projet
        JButton bSelect = new JButton("Projet analysé :");
        bSelect.setBounds(10, 50, 180, 30);
        bSelect.addActionListener(e -> {
            if (this.selectProjectPath()) {
            	this.creerMenuPrincipal();
            }
        });
        this.page.add(bSelect);

		// afficher la question 1 : deux select de classes, et l'affichage de la donnée
        TypeDeclaration[] classesArray = this.controller.getAllClasses().toArray(new TypeDeclaration[0]);
        JComboBox<TypeDeclaration> comboBoxA = new JComboBox<>(classesArray);
        comboBoxA.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                TypeDeclaration objet = (TypeDeclaration) value;
                return super.getListCellRendererComponent(list, objet.getName().getFullyQualifiedName(), index, isSelected, cellHasFocus);
            }
        });
        comboBoxA.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    selectedClassA = (TypeDeclaration) e.getItem();
                    displayValueCouplage();
                }
            }
        });
        if (selectedClassA != null) {
        	comboBoxA.setSelectedItem(selectedClassA);
        }
        comboBoxA.setBounds(130, 100, 300, 30);
        this.page.add(comboBoxA);
        
        JComboBox<TypeDeclaration> comboBoxB = new JComboBox<>(classesArray);
        comboBoxB.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                TypeDeclaration objet = (TypeDeclaration) value;
                return super.getListCellRendererComponent(list, objet.getName().getFullyQualifiedName(), index, isSelected, cellHasFocus);
            }
        });
        comboBoxB.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    selectedClassB = (TypeDeclaration) e.getItem();
                    displayValueCouplage();
                }
            }
        });
        if (selectedClassB != null) {
        	comboBoxB.setSelectedItem(selectedClassB);
        }
        comboBoxB.setBounds(470, 100, 300, 30);
        this.page.add(comboBoxB);
        
        couplageLabel = new JLabel("", JLabel.LEFT);
        couplageLabel.setBounds(860,100,300,30);
        couplageLabel.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(couplageLabel);
        
        JLabel couplageLabel2 = new JLabel("Valeur: ", JLabel.LEFT);
        couplageLabel2.setBounds(790,100,300,30);
        couplageLabel2.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(couplageLabel2);
        
        JLabel couplageLabel3 = new JLabel("Couplage entre ", JLabel.LEFT);
        couplageLabel3.setBounds(10,100,300,30);
        couplageLabel3.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(couplageLabel3);
		
        JLabel couplageLabel4 = new JLabel("et", JLabel.LEFT);
        couplageLabel4.setBounds(440,100,300,30);
        couplageLabel4.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(couplageLabel4);
		
		// afficher la question 2 : affichage du graphe de couplage
        JButton bQ2 = new JButton("Afficher le graphe de couplage pondéré");
        bQ2.setBounds(250, 175, 500, 40);
        bQ2.addActionListener(e -> {
            displayQ2();
        });
        this.page.add(bQ2);
        
		// afficher la question 3 : affichage du cluster
        JButton bQ3 = new JButton("Afficher le clustering hiérarchique");
        bQ3.setBounds(250, 250, 500, 40);
        bQ3.addActionListener(e -> {
            displayQ3();
        });
        this.page.add(bQ3);
		
		// afficher la question 4 : affichage des modules
        JButton bQ4 = new JButton("Afficher les groupes de classes couplées");
        bQ4.setBounds(250, 325, 500, 40);
        bQ4.addActionListener(e -> {
            displayQ4();
        });
        this.page.add(bQ4);
	}
	
	private void displayQ2() {
		createPageDisplay();
		mxGraph graph = controller.getCouplageGraph();
		
		if (graph == null) {			
			return;
		}

		mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
		layout.execute(graph.getDefaultParent());
		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		graphComponent.setBounds(20, 40, 960, 370);
		
		this.page.add(graphComponent);
		graphComponent.revalidate();
		graphComponent.repaint();
	}
	
	private void displayQ3() {
		createPageDisplay();
		mxGraph graph = controller.getClustering();

		if (graph == null) {			
			return;
		}

		mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
		layout.execute(graph.getDefaultParent());
		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		graphComponent.setBounds(20, 40, 960, 370);
		
		this.page.add(graphComponent);
		graphComponent.revalidate();
		graphComponent.repaint();
	}
	
	private void displayQ4() {
		createPageDisplay();
		
        JLabel lCp = new JLabel("Entrez une valeur de cp entre 0 et 1", JLabel.LEFT);
        lCp.setBounds(250, 45, 300, 30);
        lCp.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lCp);
		
		cpField = new JTextField(10);
		cpField.setText(String.valueOf(0.001f));
		cpField.setBounds(500, 45, 100, 30);
		this.page.add(cpField);
		
        JButton bCp = new JButton("Identifier des modules");
        bCp.setBounds(30, 45, 180, 30);
        bCp.addActionListener(e -> {
        	Float cp;
        	try {
		        cp = Float.parseFloat(cpField.getText());
		        if (cp < 0 || cp > 1) {
		        	cp = 0.001f;
					cpField.setText(String.valueOf(cp));
		        }
			} catch (Exception e1) {
				cp = 0.001f;
				cpField.setText(String.valueOf(cp));
			}
        	
        	mxGraph graph = controller.getModulePartition(cp);

    		if (graph == null) {
    			return;
    		}

    		mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
    		layout.execute(graph.getDefaultParent());
    		mxGraphComponent graphComponent = null;
    		
			for (Component comp : page.getComponents()) {
			    if (comp instanceof mxGraphComponent) {
			        graphComponent = (mxGraphComponent) comp;
			        break;
			    }
			}
			
			if (graphComponent != null) {
				graphComponent.setGraph(graph);
			} else {
				graphComponent = new mxGraphComponent(graph);
	    		graphComponent.setBounds(20, 80, 960, 330);
	    		
	    		page.add(graphComponent);
			}
    		
    		graphComponent.revalidate();
    		graphComponent.repaint();
        });
        this.page.add(bCp);
	}
	
	private void createPageDisplay() {
		resetPage();
		addTitle();
		addReturnButton();
	}
	
	private void addTitle() {
		JLabel header = new JLabel("Application d'analyse de couplage C O D E P R O B E ", JLabel.CENTER);
        header.setBounds(0, 10, 1000, 30);
        header.setFont(new Font("Serif", Font.BOLD, 20));
        this.page.add(header);
	}
	
	private void addReturnButton() {
		JButton bReturn = new JButton("Revenir au menu principal");
		bReturn.setBounds(250, 420, 500, 30);
		bReturn.addActionListener(e -> {
            creerMenuPrincipal();
        });
        this.page.add(bReturn);
	}
	
	private void displayValueCouplage() {
		if (
				selectedClassA == null ||
				selectedClassB == null ||
				couplageLabel == null
				)
			return;
		Float value = controller.getCouplage(selectedClassA, selectedClassB);
		couplageLabel.setText(String.format("%.3f", value));
				
	}
	
	private boolean selectProjectPath() {
		JFileChooser fileChooser = new JFileChooser(new File("."));
	    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnValue = fileChooser.showOpenDialog(null);
	    
	    if (returnValue == JFileChooser.APPROVE_OPTION) {
	        File selectedFile = fileChooser.getSelectedFile();
	        projectPath = selectedFile.getAbsolutePath();
	        controller.setProjectPath(projectPath);
	        controller.initialize();
	        selectedClassA = null;
	        selectedClassB = null;
	        return true;
	    }
	    
	    return false;
	}
	
	private void resetPage() {
        this.page = new JPanel();
        this.page.setSize(1000, 500);
        this.page.setLayout(null);
        this.frame.setContentPane(page);
    }
	
	public void start() {
		this.frame = new JFrame("CodeProbe");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(1000, 500);
        this.creerMenuPrincipal();
        this.frame.setVisible(true);
	}
}

package app;

public class App {
	public static void main(String[] args) {
		String jrePath = System.getProperty("java.home");
		
		CodeProbeI controller = new CodeProbeController(jrePath);
		
		GUI gui = new GUI(controller);
		gui.start();
	}
}

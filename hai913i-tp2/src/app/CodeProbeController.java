package app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;

import clustering.controller.Clustering;
import clustering.controller.CouplingOptimizedClusterPartitioning;
import clustering.exceptions.ClusterNotFoundException;
import clustering.exceptions.TotalRelationsMustBeStrictlyPositiveException;
import clustering.models.ClassCallGraph;
import clustering.models.Cluster;
import clustering.models.ElemCluster;
import models.CallGraph;
import models.CouplageGraph;
import parser.JDTASTParser;
import parser.Parser;

public class CodeProbeController implements CodeProbeI {
	private String projectPath;
	private String jrePath;
	private Parser parser;
	private CallGraph callGraph;
	private CouplageGraph couplageGraph;
	private Clustering clustering;
	private CouplingOptimizedClusterPartitioning partitioning;
	
	public CodeProbeController(String projectPath, String jrePath) {
		this.setProjectPath(projectPath);
		this.setJrePath(jrePath);
		this.initialize();
	}
	
	public CodeProbeController(String jrePath) {
		this.setJrePath(jrePath);
	}
	
	public CodeProbeController() {}
	
	public void setProjectPath(String path) {
		this.projectPath = path;	
	}

	public void setJrePath(String path) {
		this.jrePath = path;		
	}

	public void initialize() {
		this.parser = new JDTASTParser(projectPath, jrePath);
		parser.configure();
		parser.initialize();
		this.callGraph = null;
		this.couplageGraph = null;
		this.clustering = null;
		this.partitioning = null;
	}

	public List<TypeDeclaration> getAllClasses() {
		checkCallGraph();
		return this.callGraph.getClasses();
	}

	public float getCouplage(TypeDeclaration classA, TypeDeclaration classB) {
		checkCouplageGraph();
		return this.couplageGraph.getCouplage(classA, classB);
	}

	public mxGraph getCouplageGraph() {
		checkCouplageGraph();
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		graph.getModel().beginUpdate();
		
		Map<TypeDeclaration, Object> vertexes = new HashMap<TypeDeclaration, Object>();
		for (TypeDeclaration class_: this.getAllClasses()) {
			String name = class_.getName().getFullyQualifiedName();
			Object v = graph.insertVertex(parent, null, name, 0, 0, name.length()*7, 30);
			vertexes.put(class_, v);
		}
		
		for (TypeDeclaration classA: this.getAllClasses()) {
			Object v1 = vertexes.get(classA);
			if (couplageGraph.graph.get(classA) == null) {
				continue;
			}
			for (TypeDeclaration classB: couplageGraph.graph.get(classA).keySet()) {
				Object v2 = vertexes.get(classB);
				Float value = couplageGraph.graph.get(classA).get(classB);
				if (value > 0) {
					graph.insertEdge(parent, null, String.format("%.3f", value), v1, v2);
				}
			}
		}
		
		graph.getModel().endUpdate();
		return graph;
	}

	public mxGraph getClustering() {
		checkClustering();
		Cluster cluster;
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		try {
			cluster = clustering.getCluster();
		} catch (ClusterNotFoundException e) {
			return null;
		}
		
		graph.getModel().beginUpdate();
		
		Map<ElemCluster, Object> vertexes = new HashMap<ElemCluster, Object>();
		recursiveInsertVertex(vertexes, graph, cluster, parent);
		
		recursiveInsertEdge(vertexes, graph, cluster, parent);
		
		graph.getModel().endUpdate();
		return graph;
	}

	public mxGraph getModulePartition(float cp) {
		checkPartitioning();
		List<Cluster> modules = partitioning.getModulePartition(cp);
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		
		graph.getModel().beginUpdate();
		
		Map<ElemCluster, Object> vertexes = new HashMap<ElemCluster, Object>();
		for (Cluster c: modules) {
			recursiveInsertVertex(vertexes, graph, c, parent);
			
			recursiveInsertEdge(vertexes, graph, c, parent);
		}
		
		graph.getModel().endUpdate();
		return graph;
	}
	
	private void recursiveInsertVertex(Map<ElemCluster, Object> vertexes, mxGraph graph, ElemCluster cluster, Object parent) {
		String name = "";
		if (cluster instanceof ClassCallGraph) {
			name = ((ClassCallGraph)cluster).getClassType().getName().getFullyQualifiedName();
		}
		Object v = graph.insertVertex(parent, null, name, 0, 0, name.length()*7, 30);
		vertexes.put(cluster, v);
		
		if (cluster instanceof ClassCallGraph) {
			return;
		}
		
		Cluster c = (Cluster)cluster;
		
		recursiveInsertVertex(vertexes, graph, c.getElemA(), parent);
		recursiveInsertVertex(vertexes, graph, c.getElemB(), parent);
	}
	
	private void recursiveInsertEdge(Map<ElemCluster, Object> vertexes, mxGraph graph, ElemCluster cluster, Object parent) {
		if (cluster instanceof ClassCallGraph) {
			return;
		}
		
		Cluster c = (Cluster)cluster;
		ElemCluster a = c.getElemA();
		ElemCluster b = c.getElemB();
		
		Object vc = vertexes.get(c);
		Object va = vertexes.get(a);
		Object vb = vertexes.get(b);
		
		Object e1 = graph.insertEdge(parent, null, "", vc, va);
		Object e2 = graph.insertEdge(parent, null, "", vc, vb);
		
		graph.setCellStyle("endArrow=none", new Object[]{e1});
		graph.setCellStyle("endArrow=none", new Object[]{e2});
		
		recursiveInsertEdge(vertexes, graph, a, parent);
		recursiveInsertEdge(vertexes, graph, b, parent);
	}
	
	private void checkCallGraph() {
		if (this.callGraph == null) {
			this.callGraph = this.parser.getCallGraph();
		}
	}

	private void checkCouplageGraph() {
		checkCallGraph();
		if (this.couplageGraph == null) {
			this.couplageGraph = this.callGraph.getCouplageGraph();
		}
	}
	
	private void checkClustering() {
		checkCouplageGraph();
		if (this.clustering == null) {
			this.clustering = new Clustering(callGraph);
		}
	}
	
	private void checkPartitioning() {
		checkClustering();
		if (this.partitioning == null) {
			int totalRelations = callGraph.getTotalNumberOfRelations();
			try {
				this.partitioning =  new CouplingOptimizedClusterPartitioning(clustering.getCluster(), totalRelations);
			} catch (TotalRelationsMustBeStrictlyPositiveException | ClusterNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}

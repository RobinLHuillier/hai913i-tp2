package parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;

import exceptions.FolderInvalidException;
import models.CallGraph;

public class JDTASTParser implements Parser {
	private String projectPath;
	private String srcPath = "/src";
	private String projectSourcePath = projectPath + srcPath;
	private String jrePath;
	private String[] encoding = new String[] { "UTF-8" };
	private boolean includeRunningVMBootclasspath = true;
	private ArrayList<CompilationUnit> units;
	
	public JDTASTParser(String projectPath, String jrePath) {
		setProjectPath(projectPath);
		setJrePath(jrePath);
	}
	
	public JDTASTParser() {}
	
	public void configure() {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.contains("win")) {
			srcPath = "\\src";
		} else {	
			srcPath = "/src";
		}
		encoding = new String[] { "UTF-8" };
		includeRunningVMBootclasspath = true;
	}
	
	public void initialize() {
		createAllParsers();
	}
	
	public void setProjectPath(String path) {
		projectPath = path;
		projectSourcePath = projectPath + srcPath;
	}
	
	public void setJrePath(String path) {
		jrePath = path;
	}
	
	public String getProjectPath() {
		return projectPath;
	}
	
	public String getJrePath() {
		return jrePath;
	}
	
	public CallGraph getCallGraph() {
		ClassDeclarationVisitor visitorClass = new ClassDeclarationVisitor();
		visitAll(visitorClass);
		MethodDeclarationVisitor visitor = new MethodDeclarationVisitor(visitorClass.getClasses());
		visitAll(visitor);
		return visitor.getMethodsMap();
	}
	
	private void visitAll(ASTVisitor visitor) {
		for (CompilationUnit unit: units) {
			unit.accept(visitor);
		}
	}
	
	private void createAllParsers() {
		final File folder = new File(projectSourcePath);
		ArrayList<File> javaFiles = new ArrayList<>();
		try {
			javaFiles = listJavaFilesForFolder(folder);
		} catch (FolderInvalidException e) {
			e.printStackTrace();
		}
		
		units = new ArrayList<CompilationUnit>();
		for (File fileEntry: javaFiles) {
			String content;
			try {
				content = FileUtils.readFileToString(fileEntry);
				units.add(parse(content.toCharArray()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private CompilationUnit parse(char[] classSource) {
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		configureParser(parser, classSource);
		return (CompilationUnit)parser.createAST(null);
	}
	
	private ArrayList<File> listJavaFilesForFolder(final File folder) throws FolderInvalidException {
		if (folder == null || !folder.isDirectory()) {
			throw new FolderInvalidException();
		}
		ArrayList<File> javaFiles = new ArrayList<File>();
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				javaFiles.addAll(listJavaFilesForFolder(fileEntry));
			} else if (fileEntry.getName().contains(".java")) {
				javaFiles.add(fileEntry);
			}
		}

		return javaFiles;
	}
	
	private void configureParser(ASTParser parser, char[] classSource) {
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setBindingsRecovery(true);
		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);
		parser.setUnitName("");
		
		String[] sources = { projectSourcePath }; 
		String[] classpath = { jrePath };
 
		parser.setEnvironment(classpath, sources, encoding, includeRunningVMBootclasspath);
		parser.setSource(classSource);
	}
}

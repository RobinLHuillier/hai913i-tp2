package parser;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import models.CallGraph;

public class MethodDeclarationVisitor extends ASTVisitor {
	CallGraph callMap = new CallGraph();
	
	public MethodDeclarationVisitor(List<TypeDeclaration> classes) {
		callMap.addClasses(classes);
	}

	public boolean visit(MethodDeclaration node) {		
		MethodInvocationVisitor methodInvocationVisitor = new MethodInvocationVisitor();
        node.accept(methodInvocationVisitor);
        callMap.addMethodInvocations(node, methodInvocationVisitor.getMethods());
		
		return super.visit(node);
	}
	
	public CallGraph getMethodsMap() {
		return callMap;
	}
}

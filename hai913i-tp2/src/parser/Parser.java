package parser;

import models.CallGraph;

public interface Parser {
	
	public void configure();
	public void initialize();
	
	public String getJrePath();
	public String getProjectPath();
	public void setJrePath(String path);
	public void setProjectPath(String path);
	
	public CallGraph getCallGraph();
}

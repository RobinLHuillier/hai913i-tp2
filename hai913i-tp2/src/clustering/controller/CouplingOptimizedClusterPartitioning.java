package clustering.controller;

import java.util.ArrayList;
import java.util.List;

import clustering.exceptions.ClusterNotFoundException;
import clustering.exceptions.TotalRelationsMustBeStrictlyPositiveException;
import clustering.models.ClassCallGraph;
import clustering.models.Cluster;
import clustering.models.ElemCluster;

public class CouplingOptimizedClusterPartitioning {
	private Cluster cluster;
	private int totalRelations = 1;
	
	public CouplingOptimizedClusterPartitioning(Cluster cluster, int totalRelations) throws TotalRelationsMustBeStrictlyPositiveException {
		this.cluster = cluster;
		if (totalRelations == 0) {
			throw new TotalRelationsMustBeStrictlyPositiveException();
		}
		this.totalRelations = totalRelations;
	}
	
	public List<Cluster> getModulePartition(float cp) {
		List<ElemCluster> clusters = new ArrayList<>();
		clusters.add(cluster);
		return getSmallestPartition(cp, clusters);
	}
	
	private List<Cluster> getSmallestPartition(float cp, List<ElemCluster> listClusters) {
		List<Cluster> result = new ArrayList<>();
		for (ElemCluster elem: listClusters) {
			if (!(elem instanceof Cluster)) {
				continue;
			}
			Cluster c = (Cluster) elem;
			float avgCouplage = getAvgCouplage(c);
			ElemCluster a = c.getElemA();
			ElemCluster b = c.getElemB();
			if (
					avgCouplage < cp
//					|| (a instanceof Cluster && getAvgCouplage((Cluster)a) > cp)
//					|| (b instanceof Cluster && getAvgCouplage((Cluster)b) > cp)
			) {
				List<ElemCluster> nc = new ArrayList<>();
				nc.add(a);
				nc.add(b);
				result.addAll(getSmallestPartition(cp, nc));
			} else {
				result.add(c);
			}
		}
		return result;
	}
	
	private float getAvgCouplage(Cluster c) {
		float totalCouplage = 0;
		int count = 0;
		List<ClassCallGraph> l = c.getClassCallGraph();
		for (int i=0; i<l.size()-1; i++) {
			ClassCallGraph c1 = l.get(i);
			for (int j=i+1; j<l.size(); j++) {
				ClassCallGraph c2 =  l.get(j);
				totalCouplage += ((float)c1.countCallsToClasses(c2.getClasses()) / totalRelations);
				count++;
			}
		}
		
		return (float)totalCouplage/count;
	}
	
	public boolean isModulePartitionValid(List<Cluster> l) throws ClusterNotFoundException {
		if (cluster == null) {
			throw new ClusterNotFoundException();
		}	
		boolean lessThanHalfClassesAreModules = ((float)cluster.getClasses().size()/2) > l.size();
		int countClasses = 0;
		for (Cluster c: l) {
			countClasses += c.getClasses().size();
		}
		boolean allClassesAreInModules = cluster.getClasses().size() == countClasses;
		
		System.out.println(l.size());
		
		return lessThanHalfClassesAreModules && allClassesAreInModules;
	}
}

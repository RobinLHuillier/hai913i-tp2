package clustering.controller;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import clustering.exceptions.ClusterNotFoundException;
import clustering.models.ClassCallGraph;
import clustering.models.Cluster;
import clustering.models.ElemCluster;
import models.CallGraph;

public class Clustering {
	private List<ElemCluster> elems;
	
	public Clustering(CallGraph graph) {
		elems = new ArrayList<ElemCluster>();
		transformCallGraph(graph);
		cluster();
	}
	
	public Cluster getCluster() throws ClusterNotFoundException {
		if (elems.size() < 1) {
			throw new ClusterNotFoundException();
		}
		if (elems.size() > 1) {
			cluster();
		}
		return (Cluster)elems.get(0);
	}
	
	private void cluster() {
		while (elems.size() > 1) {
			Cluster c = findAndClusterMostCoupledElements();
			elems.remove(c.getElemA());
			elems.remove(c.getElemB());
			elems.add(c);
		}
	}
	
	private Cluster findAndClusterMostCoupledElements() {
		int maxCouplage = 0;
		ElemCluster elemA = elems.get(0);
		ElemCluster elemB = elems.get(1);
		for (int i = 0; i < elems.size() -1; i++) {
			ElemCluster a = elems.get(i);
			for (int j = i+1; j < elems.size(); j++) {
				ElemCluster b = elems.get(j);
				int couplage = getCouplage(a, b);
				if (couplage > maxCouplage) {
					maxCouplage = couplage;
					elemA = a;
					elemB = b;
				}
			}
		}
		return new Cluster(elemA, elemB, maxCouplage);
	}
	
	private int getCouplage(ElemCluster a, ElemCluster b) {
		List<TypeDeclaration> classesA = a.getClasses();
		List<TypeDeclaration> classesB = b.getClasses();
		return a.countCallsToClasses(classesB) + b.countCallsToClasses(classesA);
	}
	
	private void transformCallGraph(CallGraph graph) {
		for (TypeDeclaration class_: graph.getClasses()) {
			ClassCallGraph c = new ClassCallGraph(graph.getMethodsFromClass(class_), class_);
			elems.add(c);
		}
	}
	
	public String toString() {
		try {
			return getCluster().toString();
		} catch (ClusterNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}
}

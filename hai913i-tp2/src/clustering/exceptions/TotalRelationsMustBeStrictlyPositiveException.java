package clustering.exceptions;

public class TotalRelationsMustBeStrictlyPositiveException extends Exception {

	public TotalRelationsMustBeStrictlyPositiveException() {}
	
	public TotalRelationsMustBeStrictlyPositiveException(String message) {
		super(message);
	}
}
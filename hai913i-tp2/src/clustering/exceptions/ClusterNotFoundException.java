package clustering.exceptions;

public class ClusterNotFoundException extends Exception {

	public ClusterNotFoundException() {}
	
	public ClusterNotFoundException(String message) {
		super(message);
	}
}
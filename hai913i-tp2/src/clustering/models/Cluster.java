package clustering.models;

import java.util.List;

import org.eclipse.jdt.core.dom.TypeDeclaration;

public class Cluster implements ElemCluster {
	private ElemCluster elemA;
	private ElemCluster elemB;
	private int clusterValue = 0;
	
	public Cluster() {}
	
	public Cluster(ElemCluster a, ElemCluster b, int v) {
		this.elemA = a;
		this.elemB = b;
		this.clusterValue = v;
	}
	
	public ElemCluster getElemA() {
		return elemA;
	}
	
	public void setElemA(ElemCluster elemA) {
		this.elemA = elemA;
	}
	
	public ElemCluster getElemB() {
		return elemB;
	}
	
	public void setElemB(ElemCluster elemB) {
		this.elemB = elemB;
	}	
	
	public int getClusterValue() {
		return this.clusterValue;
	}
	
	public void setClusterValue(int value) {
		this.clusterValue = value;
	}
	
	public List<TypeDeclaration> getClasses() {
		List<TypeDeclaration> a = elemA.getClasses();
		a.addAll(elemB.getClasses());
		return a;
	}
	
	public List<ClassCallGraph> getClassCallGraph() {
		List<ClassCallGraph> a = elemA.getClassCallGraph();
		a.addAll(elemB.getClassCallGraph());
		return a;
	}
	
	public int countCallsToClasses(List<TypeDeclaration> classes) {
		return elemA.countCallsToClasses(classes) + elemB.countCallsToClasses(classes);
	}
	
	public String toString() {
		return "(" + getClusterValue() + " : " + elemA.toString() + " - " + elemB.toString() + " )";
	}
}

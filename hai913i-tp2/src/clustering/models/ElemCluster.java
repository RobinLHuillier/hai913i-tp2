package clustering.models;

import java.util.List;

import org.eclipse.jdt.core.dom.TypeDeclaration;

public interface ElemCluster {
	public List<TypeDeclaration> getClasses();
	public List<ClassCallGraph> getClassCallGraph();
	public int countCallsToClasses(List<TypeDeclaration> classes);
}

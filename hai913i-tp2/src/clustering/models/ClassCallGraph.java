package clustering.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import models.MethodDeclarationPair;
import models.MethodInvocationPair;
import utils.Utils;

public class ClassCallGraph implements ElemCluster {
	private Map<MethodDeclarationPair, List<MethodInvocationPair>> graph = new HashMap<MethodDeclarationPair, List<MethodInvocationPair>>();
	private TypeDeclaration class_;
	
	public ClassCallGraph(Map<MethodDeclarationPair, List<MethodInvocationPair>> graph, TypeDeclaration class_) {
		this.graph = graph;
		this.class_ = class_;
	}
	
	public void addMethodInvocations(MethodDeclarationPair methodD, List<MethodInvocationPair> methodI) {
		graph.put(methodD, methodI);
	}
	
	public int countCallToClass(TypeDeclaration class_) {
		int count = 0;
		for (MethodDeclarationPair m: getKeys()) {
			for(MethodInvocationPair mi: graph.get(m)) {
				if (Utils.checkEqualityTypeDeclaration(class_, mi.getClassType())) {
					count++;
				}
			}
		}
		return count;
	}
	
	public List<MethodDeclarationPair> getKeys() {
		return new ArrayList<>(graph.keySet());
	}
	
	public List<TypeDeclaration> getClasses() {
		List<TypeDeclaration> l = new ArrayList<>();
		l.add(getClassType());
		return l;
	}
	
	public List<ClassCallGraph> getClassCallGraph() {
		List<ClassCallGraph> l = new ArrayList<>();
		l.add(this);
		return l;
	}
	
	public int countCallsToClasses(List<TypeDeclaration> classes) {
		int count = 0;
		for (TypeDeclaration class_: classes) {
			count += countCallToClass(class_);
		}
		return count;
	}
	
	public TypeDeclaration getClassType() {
		return this.class_;
	}
	
	public String toString() {
		return getClassType().getName().getFullyQualifiedName();
	}
}

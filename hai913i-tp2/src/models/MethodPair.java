package models;

import org.eclipse.jdt.core.dom.TypeDeclaration;

public abstract class MethodPair {
	private TypeDeclaration classType;
	
	public MethodPair(TypeDeclaration classType) {
		setClassType(classType);
	}
	
	public void setClassType(TypeDeclaration classType) {
		this.classType = classType;
	}
	
	public TypeDeclaration getClassType() {
		return classType;
	}
	
	public String getClassName() {
		return classType.getName().getFullyQualifiedName();
	}
	
	public String toString() {
		return getClassName();
	}
}

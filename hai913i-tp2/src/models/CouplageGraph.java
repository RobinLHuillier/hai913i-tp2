package models;

import java.util.HashMap;

import java.util.Map;

import utils.Utils;

import org.eclipse.jdt.core.dom.TypeDeclaration;

public class CouplageGraph {
	public Map<TypeDeclaration, Map<TypeDeclaration, Float>> graph;
	
	public CouplageGraph() {
		graph = new HashMap<TypeDeclaration, Map<TypeDeclaration, Float>>();
	}
	
	public void add(TypeDeclaration classA, TypeDeclaration classB, Float couplage) {
		if (graph.get(classA) == null) {
			graph.put(classA, new HashMap<TypeDeclaration, Float>());
		}
		graph.get(classA).put(classB, couplage);
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		
		for (TypeDeclaration classA: graph.keySet()) {
			for (TypeDeclaration classB: graph.get(classA).keySet()) {
				float couplage = graph.get(classA).get(classB);
				s.append("Couplage entre " + classA.getName().getFullyQualifiedName() + " et " + classB.getName().getFullyQualifiedName() +  " : " + couplage + "\n");
			}
		}
		
		return s.toString();
	}
	
	public Float getCouplage(TypeDeclaration classA, TypeDeclaration classB) {
		if (Utils.checkEqualityTypeDeclaration(classA, classB)) {
			return (float) 1;
		}
		if (graph.get(classA) == null) {
			if (graph.get(classB) == null) {
				return (float) 0;
			}
			return getCouplage(classB, classA);
		}
		Float f = graph.get(classA).get(classB);
		return f == null ? 0f : f;
	}
}

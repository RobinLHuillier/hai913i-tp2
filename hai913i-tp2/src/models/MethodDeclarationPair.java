package models;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class MethodDeclarationPair extends MethodPair {
	private MethodDeclaration method;
	
	public MethodDeclarationPair(TypeDeclaration classType, MethodDeclaration method) {
		super(classType);
		setMethod(method);
	}
	
	public void setMethod(MethodDeclaration method) {
		this.method = method;
	}
	
	public MethodDeclaration getMethod() {
		return method;
	}
	
	public String getMethodName() {
		return method.getName().getFullyQualifiedName();
	}
	
	public String toString() {
		return super.toString() + "." + getMethodName();
	}
}

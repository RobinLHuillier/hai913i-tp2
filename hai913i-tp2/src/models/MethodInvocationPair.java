package models;

import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class MethodInvocationPair extends MethodPair {
	private MethodInvocation method;
	
	public MethodInvocationPair(TypeDeclaration classType, MethodInvocation method) {
		super(classType);
		setMethod(method);
	}
	
	public void setMethod(MethodInvocation method) {
		this.method = method;
	}
	
	public MethodInvocation getMethod() {
		return method;
	}
	
	public String getMethodName() {
		return method.getName().getFullyQualifiedName();
	}
	
	public String toString() {
		return super.toString() + "." + getMethodName();
	}
}

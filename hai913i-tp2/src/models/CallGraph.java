package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import utils.Utils;

public class CallGraph {
	private Map<MethodDeclarationPair, List<MethodInvocationPair>> graph;
	private List<TypeDeclaration> classes;
	
	public CallGraph() {
		graph = new HashMap<MethodDeclarationPair, List<MethodInvocationPair>>();
		classes = new ArrayList<TypeDeclaration>();
	}

	public void addClasses(List<TypeDeclaration> classes) {
		this.classes = classes;
	}
	
	public void addMethodInvocations(MethodDeclaration methodD, List<MethodInvocation> methodI) {
		MethodDeclarationPair mdPair= new MethodDeclarationPair(getClassType(methodD), methodD);
		List<MethodInvocationPair> l = new ArrayList<MethodInvocationPair>();
		for (MethodInvocation method: methodI) {
			TypeDeclaration classType = getClassType(method);
			if (classType != null) {
				// on évite les méthodes non écrites dans le projet (par exemple Arrays.asList() )
				l.add(new MethodInvocationPair(classType, method));
			}
		}
		graph.put(mdPair, l);
	}
	
	public int getTotalNumberOfRelations() {
		int count = 0;
		for (MethodDeclarationPair key: getKeys()) {
			count += graph.get(key).size();
		}
		return count;
	}
	
	public TypeDeclaration getClassWithName(String className) {
		for (TypeDeclaration classType: classes) {
			if (className.equals(classType.getName().getFullyQualifiedName())) {
				return classType;
			}
		}
		return null;
	}
	
	public float getCouplage(TypeDeclaration classA, TypeDeclaration classB) {
		int totalCount = getNumberOfCallFromAtoB(classA, classB) + getNumberOfCallFromAtoB(classB, classA);
		return (float)totalCount/getTotalNumberOfRelations();
	}
	
	public CouplageGraph getCouplageGraph() {
		CouplageGraph cGraph = new CouplageGraph();
		for (int i = 0; i < classes.size()-1; i++) {
			TypeDeclaration classA = classes.get(i);
			for (int j = i+1; j < classes.size(); j++) {
				TypeDeclaration classB = classes.get(j);
				if (Utils.checkEqualityTypeDeclaration(classA, classB)) {
					continue;
				}
				float couplage = getCouplage(classA, classB);
				cGraph.add(classA, classB, couplage);
			}
		}
		return cGraph;
	}
	
	public Map<MethodDeclarationPair, List<MethodInvocationPair>> getMethodsFromClass(TypeDeclaration class_) {
		Map<MethodDeclarationPair, List<MethodInvocationPair>> map = new HashMap<MethodDeclarationPair, List<MethodInvocationPair>>();
		for (MethodDeclarationPair m: getKeys()) {
			if (Utils.checkEqualityTypeDeclaration(class_, m.getClassType())) {
				map.put(m, graph.get(m));
			}
		}
		return map;
	}
	
	public List<TypeDeclaration> getClasses() {
		return classes;
	}
	
	private int getNumberOfCallFromAtoB(TypeDeclaration classA, TypeDeclaration classB) {
		int count = 0;
		for (MethodDeclarationPair mdPair: getKeys()) {
			if (Utils.checkEqualityTypeDeclaration(mdPair.getClassType(), classA)) {
				for (MethodInvocationPair miPair: graph.get(mdPair)) {
					if (Utils.checkEqualityTypeDeclaration(miPair.getClassType(), classB)) {
						count++;
					}
				}
			}
		}
		return count;
	}
	
	private TypeDeclaration getClassType(MethodDeclaration method) {
		ASTNode parent = method.getParent();
	    while (!(parent instanceof TypeDeclaration)) {
	        parent = parent.getParent();
	    }
	    TypeDeclaration typeDecl = (TypeDeclaration) parent;
	    return typeDecl;
	}
	
	private TypeDeclaration getClassType(MethodInvocation method) {
        IMethodBinding methodBinding = method.resolveMethodBinding();
        if (methodBinding != null) {
        	return getClassWithName(methodBinding.getDeclaringClass().getName());
        }
        return null;
	}
	
	private List<MethodDeclarationPair> getKeys() {
		return new ArrayList<>(graph.keySet());
	}
}
